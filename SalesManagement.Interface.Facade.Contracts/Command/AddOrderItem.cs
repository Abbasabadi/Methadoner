﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Framework.Application;

namespace SalesManagement.Interface.Facade.Contracts.Command
{
    public class AddOrderItem : ICommand
    {
        public long OrderId { get; set; }
        public long Goods { get; set; }
        public long Number { get; set; }
        public double TotalPrice { get; set; }
    }
}