﻿using System;
using Framework.Application;

namespace SalesManagement.Interface.Facade.Contracts.Command
{
    public class AddOrder : ICommand
    {
        public string CustomerName { get; set; }
        public DateTime RegisterDatetime { get; set; }

        public AddOrder()
        {
            
        }

        public AddOrder(string customerName, DateTime registerDatetime)
        {
            CustomerName = customerName;
            RegisterDatetime = registerDatetime;
        }
    }
}