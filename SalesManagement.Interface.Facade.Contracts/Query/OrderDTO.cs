﻿using System.Collections.Generic;

namespace SalesManagement.Interface.Facade.Contracts.Query
{
    public class OrderDto
    {
        public long Id { get; set; }
        public string CustomerName { get; set; }
        public string RegisterDatetime { get; set; }
        public List<OrderItemsDto> Items { get; set; }
        public double Ammount { get; set; }
    }
}