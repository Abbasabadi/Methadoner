﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesManagement.Interface.Facade.Contracts.Query
{
    public class OrderItemsDto
    {
        public long Number { get; set; }
        public string Goods { get; set; }
        public double TotalPrice { get; set; }
    }
}
