﻿namespace SalesManagement.Interface.Facade.Contracts.Query
{
    public class GoodsDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }

        public GoodsDto(long id, string name, double price)
        {
            Id = id;
            Name = name;
            Price = price;
        }
    }
}