﻿using SalesManagement.Interface.Facade.Contracts.Command;

namespace SalesManagement.Interface.Facade.Contracts
{
    public interface IOrderCommandFacade
    {
        long AddOrder(AddOrder command);
        void AddOrderItem(AddOrderItem command);
    }
}
