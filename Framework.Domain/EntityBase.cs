﻿namespace Framework.Domain
{
    public abstract class EntityBase<T>
    {
        public T EntityId { get; protected set; }

        protected EntityBase()
        {
        }

        protected EntityBase(T entityId)
        {
            EntityId = entityId;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj.GetType() != this.GetType()) return false;

            var entity = obj as EntityBase<T>;
            return EntityId.Equals(entity.EntityId);
        }

        public override int GetHashCode()
        {
            return EntityId.GetHashCode();
        }
    }
}