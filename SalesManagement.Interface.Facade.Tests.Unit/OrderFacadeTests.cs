﻿using Framework.Application;
using Framework.Core.Events;
using Moq;
using SalesManagement.Domain.Contract;
using SalesManagement.Interface.Facade.Contracts.Command;
using Xunit;

namespace SalesManagement.Interface.Facade.Tests.Unit
{
    public class OrderFacadeTests
    {
        private readonly Mock<ICommandBus> _commandBus;

        public OrderFacadeTests()
        {
            _commandBus = new Mock<ICommandBus>();
        }

        [Fact]
        public void AddOrder_Should_Call_Dispatch_When_AddOrder_Command_Passed()
        {
            //Arrange
            var command = AddOrderCommand();
            var eventListener = new Mock<IEventListener>();
            var facade = new OrderCommandFacade(_commandBus.Object, eventListener.Object);

            //Act
            facade.AddOrder(command);

            //Assert
            _commandBus.Verify(a => a.Dispatch(command));
        }


        [Fact]
        public void AddOrder_Should_Return_Added_Order_Id_When_Command_Passed()
        {
            const long orderId = 1;
            var command = AddOrderCommand();
            var eventAggregator = new EventAggregator();
            var expectedEvent = new OrderCreated(orderId);
            _commandBus.Setup(x => x.Dispatch(command)).Callback(() => eventAggregator.Publish(expectedEvent));
            var facade = new OrderCommandFacade(_commandBus.Object, eventAggregator);

            var result = facade.AddOrder(command);

            Assert.Equal(expectedEvent.Id, result);
        }

        private static AddOrder AddOrderCommand()
        {
            return new AddOrder();
        }


        [Fact]
        public void AddOrderItem_Should_Call_CommandBus_When_Command_Passed()
        {
            //Arrange
            var command = new AddOrderItem();
            var eventListener = new Mock<IEventListener>();
            var facade = new OrderCommandFacade(_commandBus.Object, eventListener.Object);

            //Act
            facade.AddOrderItem(command);

            //Assert
            _commandBus.Verify(a=>a.Dispatch(command));
        }
    }
}