﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.Core.Events;
using Framework.Domain;
using SalesManagement.Domain.Contract;

namespace SalesManagement.Domain.Orders
{
    public class Order : AggregateRootBase<OrderId>
    {
        public IList<OrderItem> _items;
        public string CustomerName { get; private set; }
        public DateTime RegisterDatetime { get; private set; }
        public IReadOnlyCollection<OrderItem> Items => new ReadOnlyCollection<OrderItem>(_items);

        protected Order()
        {
        }

        public Order(OrderId id, string customerName, DateTime registerDatetime, IEventPublisher publisher) : base(id,
            publisher)
        {
            CustomerName = customerName;
            _items = new List<OrderItem>();
            RegisterDatetime = registerDatetime;

            Publish(new OrderCreated(id.DbId));
        }

        public void AddItem(OrderItem orderItem)
        {
            _items.Add(orderItem);
        }
    }
}