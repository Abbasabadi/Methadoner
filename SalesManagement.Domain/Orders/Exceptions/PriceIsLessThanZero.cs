﻿using System;

namespace SalesManagement.Domain.Orders.Exceptions
{
    public class PriceIsLessThanZero : Exception
    {
    }
}