﻿using System;

namespace SalesManagement.Domain.Orders.Exceptions
{
    public class OrderDoesNotHaveAnyItem : Exception
    {
    }
}