﻿namespace SalesManagement.Domain.Orders
{
    public interface IOrderRepository
    {
        void AddOrder(Order order);
        void Modify(Order order);
        Order GetBy(OrderId id);
        long GetNextId();
    }
}
