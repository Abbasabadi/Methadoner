﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Framework.Domain;

namespace SalesManagement.Domain.Orders
{
    public class OrderId : IdBase<long>
    {
        protected OrderId()
        {
        }

        public OrderId(long dbid) : base(dbid)
        {
        }
    }
}