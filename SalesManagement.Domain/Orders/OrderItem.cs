﻿using Framework.Domain;
using SalesManagement.Domain.Orders.Exceptions;

namespace SalesManagement.Domain.Orders
{
    public class OrderItem : ValueObjectBase
    {
        public long Number { get; private set; }
        public long Goods { get; private set; }
        public double TotalPrice { get; private set; }

        protected OrderItem()
        {
        }

        public OrderItem(long number, long goods, double totalPrice)
        {
            GuardAgainstLessThanOrEqualZeroNumber(number);
            GuardAgainstLessThanZeroPrice(totalPrice);

            Number = number;
            Goods = goods;
            TotalPrice = totalPrice;
        }

        private static void GuardAgainstLessThanZeroPrice(double price)
        {
            if (price < 0)
                throw new PriceIsLessThanZero();
        }

        private static void GuardAgainstLessThanOrEqualZeroNumber(long number)
        {
            if (number <= 0)
                throw new NumberIsLessThanOrEqualZero();
        }
    }
}