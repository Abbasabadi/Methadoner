﻿using System.Windows.Forms;
using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace SalesManagement.Interface.WinForm
{
    public static class WinFormBootstrapper
    {
        public static void WireUp(IWindsorContainer container)
        {
            container.Register(Component.For<Login>());
            container.Register(Component.For<FrmOrders>());
        }
    }
}