﻿using System;
using System.Windows.Forms;
using Castle.Windsor;
using Framework.Castle;

namespace SalesManagement.Interface.WinForm
{
    public partial class Login : Form
    {
        private readonly IWindsorContainer _container;

        public Login()
        {
            _container = ContainerProvider.Get();
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {
        }

        private void BTN_Login_Click(object sender, EventArgs e)
        {
            var serviceLocator = new WindsorServiceLocator(_container);
            if (TXB_Password.Text == @"methadon")
            {
                var frmOrders = serviceLocator.Resolve<FrmOrders>();
                frmOrders.ShowDialog();
            }
            else
            {
                MessageBox.Show(@"کلمه عبور اشتباه است", @"کلمه عبور", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}