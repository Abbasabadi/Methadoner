﻿using Castle.Windsor;

namespace SalesManagement.Interface.WinForm
{
    public static class ContainerProvider
    {
        private static readonly IWindsorContainer Container = new WindsorContainer();

        public static IWindsorContainer Get()
        {
            return Container;
        }
    }
}