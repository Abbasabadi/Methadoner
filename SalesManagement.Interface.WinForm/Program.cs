﻿using System;
using System.Windows.Forms;
using Framework.Castle;
using SalesManagement.Config;

namespace SalesManagement.Interface.WinForm
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            var container = ContainerProvider.Get();
            Bootstrapper.WireUp(container);
            SalesManagementBootstrapper.WireUp(container);
            WinFormBootstrapper.WireUp(container);
            var serviceLocator = new WindsorServiceLocator(container);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(serviceLocator.Resolve<FrmOrders>());
        }
    }
}