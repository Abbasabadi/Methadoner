﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Framework.Application;
using SalesManagement.Interface.Facade.Contracts;
using SalesManagement.Interface.Facade.Contracts.Command;
using SalesManagement.Interface.Facade.Contracts.Query;
using SalesManagement.Interface.Facade.Query.Order;

namespace SalesManagement.Interface.WinForm
{
    public partial class FrmOrders : Form
    {
        private readonly IOrderCommandFacade _orderCommandFacade;
        private readonly IOrderQueryFacade _orderQueryFacade;
        private long _orderId;

        public FrmOrders(IOrderCommandFacade orderCommandFacade, IOrderQueryFacade orderQueryFacade)
        {
            _orderCommandFacade = orderCommandFacade;
            _orderQueryFacade = orderQueryFacade;
            _orderId = 0;
            InitializeComponent();
            DT_StartDate.Text = TodayDatetime.PersianStart;
            DT_EndDate.Text = TodayDatetime.PersianEnd;
            FeedOrderGridView(TodayDatetime.GregorianStart, TodayDatetime.GregorianEnd);
            CalculateSalesAmmount();
        }

        private void CalculateSalesAmmount()
        {
            double ammount = 0;
            var todayOrders = GetOrders(TodayDatetime.GregorianStart, TodayDatetime.GregorianEnd);
            foreach (var order in todayOrders)
            {
                ammount = ammount + order.Ammount;
            }

            Txb_SalesAmmount.Text = ammount.ToString("N1", CultureInfo.CreateSpecificCulture("en-US"));
        }

        private void Frm_Orders_Load(object sender, EventArgs e)
        {
            LoadGoodsTypes();
        }

        private void Register_Order_Click(object sender, EventArgs e)
        {
            var customerName = TXB_CustomerName.Text;
            if (GuardAgainsEmptyCustomerName(customerName)) return;
            var command = new AddOrder(customerName, DateTime.Now);
            try
            {
                if (IsOrderCreated())
                {
                    _orderId = _orderCommandFacade.AddOrder(command);
                    FeedOrderGridView(TodayDatetime.GregorianStart, TodayDatetime.GregorianEnd);
                }
                else
                {
                    MessageBox.Show(@"سفارش قبلی هنوز ثبت نهایی نشده است");
                }
            }
            catch
            {
                MessageBox.Show(@"مشکلی پیش آمده لطفا دوباره تلاش کنید");
            }
        }

        private static bool GuardAgainsEmptyCustomerName(string customerName)
        {
            if (!string.IsNullOrEmpty(customerName)) return false;
            MessageBox.Show(@"لطفا نام مشری را وارد کنید");
            return true;
        }

        private void BTN_Report_Click(object sender, EventArgs e)
        {
            if (GuardAgainstEmptyStartOrEndDate()) return;
            var startDate = DatetimeConvertor.ConvertToGregorianDate(DT_StartDate.Text, 00);
            var endDate = DatetimeConvertor.ConvertToGregorianDate(DT_EndDate.Text, 23);
            if (GuardAgainstBiggerStartDate(startDate, endDate)) return;
            FeedOrderGridView(startDate, endDate);
        }

        private static bool GuardAgainstBiggerStartDate(DateTime startDate, DateTime endDate)
        {
            if (startDate <= endDate) return false;
            MessageBox.Show(@"تاریخ شروع نمی تواند از تاریخ پایان بزرگتر باشد");
            return true;
        }

        private bool GuardAgainstEmptyStartOrEndDate()
        {
            if (!string.IsNullOrEmpty(DT_StartDate.Text) && !string.IsNullOrEmpty(DT_EndDate.Text)) return false;
            MessageBox.Show(@"لطفا تاریخ شروع و پایان را وارد نمایید");
            return true;
        }

        private void FeedOrderGridView(DateTime startDate, DateTime endDate)
        {
            var orders = GetOrders(startDate, endDate);
            OrdersGridView.DataSource = orders;
            OrdersGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private List<OrderDto> GetOrders(DateTime startDate, DateTime endDate)
        {
            return _orderQueryFacade.GetOrders(startDate, endDate);
        }

        private void LoadGoodsTypes()
        {
            var goodsTypes = _orderQueryFacade.GetGoods();
            Cmb_Goods.DataSource = goodsTypes;
            Cmb_Goods.DisplayMember = "Name";
            Cmb_Goods.ValueMember = "Id";
        }

        private void Cmb_GoodsType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var id = Convert.ToInt32(Cmb_Goods.SelectedValue);
            var fee = _orderQueryFacade.GetGoods().First(a => a.Id == id).Price;
            Txb_Fee.Text = fee.ToString(CultureInfo.InvariantCulture);
            Txb_TotalPrice.Text = CalculateTotalPrice(fee);
        }

        private string CalculateTotalPrice(double fee)
        {
            return (fee * (double)Nud_Number.Value).ToString(CultureInfo.InvariantCulture);
        }

        private void Nud_Number_ValueChanged(object sender, EventArgs e)
        {
            var fee = Txb_Fee.Text;
            if (string.IsNullOrEmpty(fee)) return;
            Txb_TotalPrice.Text = CalculateTotalPrice(double.Parse(fee));
        }

        private void Btn_AddOrderItem_Click(object sender, EventArgs e)
        {
            if (GaurdAgainsEmptyOrder()) return;

            var goodsId = long.Parse(Cmb_Goods.SelectedValue.ToString());
            var number = long.Parse(Nud_Number.Text);
            var totalPrice = long.Parse(Txb_TotalPrice.Text);
            var command = CreateAddOrderItemCommand(number, goodsId, totalPrice);
            try
            {
                _orderCommandFacade.AddOrderItem(command);
                FeedOrderItemsGridView();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }

        private bool GaurdAgainsEmptyOrder()
        {
            if (!IsOrderCreated()) return false;
            MessageBox.Show(@"هنوز سفارشی ایجاد نشده است. لطفا ابتدا یک سفارش ایجاد کنید");
            return true;
        }

        private bool IsOrderCreated()
        {
            return _orderId == 0;
        }

        private AddOrderItem CreateAddOrderItemCommand(long number, long goodsId, long totalPrice)
        {
            return new AddOrderItem {
                OrderId = _orderId,
                Number = number,
                Goods = goodsId,
                TotalPrice = totalPrice
            };
        }

        private void OrdersGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            FeedOrderItemsGridView();
        }

        private void FeedOrderItemsGridView()
        {
            var row = OrdersGridView.SelectedRows[0];
            var orderId = (long)row.Cells["Id"].Value;
            var order = _orderQueryFacade.GetOrder(orderId);
            OrderItems_GridView.DataSource = order.Items;
        }

        private void Btn_FinalRegisterOrder_Click(object sender, EventArgs e)
        {
            if (IsOrderCreated()) return;
            var dialogResult = MessageBox.Show(@"آیا از ثبت نهایی اطمینان دارید؟", "", MessageBoxButtons.YesNo);
            if (dialogResult != DialogResult.Yes) return;
            _orderId = 0;
            FeedOrderGridView(TodayDatetime.GregorianStart, TodayDatetime.GregorianEnd);
            ClearControlls();
            CalculateSalesAmmount();
        }

        private void ClearControlls()
        {
            TXB_CustomerName.Text = string.Empty;
            Txb_Fee.Text = string.Empty;
            Txb_TotalPrice.Text = string.Empty;
            Cmb_Goods.SelectedIndex = 0;
            Nud_Number.Text = 0.ToString();
        }
    }

    public static class TodayDatetime
    {
        public static DateTime GregorianStart = DateTime.Today.AddHours(00);
        public static DateTime GregorianEnd = DateTime.Today.AddHours(23);
        public static string PersianStart = DatetimeConvertor.ConvertToPersianDate(DateTime.Now);
        public static string PersianEnd = DatetimeConvertor.ConvertToPersianDate(DateTime.Now);
    }
}