﻿namespace SalesManagement.Interface.WinForm
{
    partial class FrmOrders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Btn_FinalRegisterOrder = new System.Windows.Forms.Button();
            this.TXB_CustomerName = new System.Windows.Forms.TextBox();
            this.Btn_AddOrder = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.OrdersGridView = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CustomerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RegisterDatetime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ammount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Btn_AddOrderItem = new System.Windows.Forms.Button();
            this.Txb_Fee = new System.Windows.Forms.TextBox();
            this.Txb_TotalPrice = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Nud_Number = new System.Windows.Forms.NumericUpDown();
            this.Cmb_Goods = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.OrderItems_GridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.BTN_Report = new System.Windows.Forms.Button();
            this.DT_EndDate = new Atf.UI.DateTimeSelector();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.DT_StartDate = new Atf.UI.DateTimeSelector();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.Txb_SalesAmmount = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OrdersGridView)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Nud_Number)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderItems_GridView)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Btn_FinalRegisterOrder);
            this.groupBox1.Controls.Add(this.TXB_CustomerName);
            this.groupBox1.Controls.Add(this.Btn_AddOrder);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 18);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(237, 102);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ایجاد سفارش";
            // 
            // Btn_FinalRegisterOrder
            // 
            this.Btn_FinalRegisterOrder.BackColor = System.Drawing.SystemColors.Desktop;
            this.Btn_FinalRegisterOrder.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Btn_FinalRegisterOrder.Location = new System.Drawing.Point(10, 65);
            this.Btn_FinalRegisterOrder.Name = "Btn_FinalRegisterOrder";
            this.Btn_FinalRegisterOrder.Size = new System.Drawing.Size(94, 30);
            this.Btn_FinalRegisterOrder.TabIndex = 3;
            this.Btn_FinalRegisterOrder.Text = "ثبت نهایی";
            this.Btn_FinalRegisterOrder.UseVisualStyleBackColor = false;
            this.Btn_FinalRegisterOrder.Click += new System.EventHandler(this.Btn_FinalRegisterOrder_Click);
            // 
            // TXB_CustomerName
            // 
            this.TXB_CustomerName.Location = new System.Drawing.Point(10, 27);
            this.TXB_CustomerName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TXB_CustomerName.Name = "TXB_CustomerName";
            this.TXB_CustomerName.Size = new System.Drawing.Size(136, 29);
            this.TXB_CustomerName.TabIndex = 2;
            // 
            // Btn_AddOrder
            // 
            this.Btn_AddOrder.Location = new System.Drawing.Point(123, 63);
            this.Btn_AddOrder.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Btn_AddOrder.Name = "Btn_AddOrder";
            this.Btn_AddOrder.Size = new System.Drawing.Size(99, 32);
            this.Btn_AddOrder.TabIndex = 1;
            this.Btn_AddOrder.Text = "ایجاد سفارش";
            this.Btn_AddOrder.UseVisualStyleBackColor = true;
            this.Btn_AddOrder.Click += new System.EventHandler(this.Register_Order_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(155, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "نام مشتری:";
            // 
            // OrdersGridView
            // 
            this.OrdersGridView.AllowDrop = true;
            this.OrdersGridView.AllowUserToAddRows = false;
            this.OrdersGridView.AllowUserToDeleteRows = false;
            this.OrdersGridView.AllowUserToOrderColumns = true;
            this.OrdersGridView.AllowUserToResizeRows = false;
            this.OrdersGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OrdersGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.CustomerName,
            this.RegisterDatetime,
            this.Ammount});
            this.OrdersGridView.GridColor = System.Drawing.SystemColors.Control;
            this.OrdersGridView.Location = new System.Drawing.Point(12, 275);
            this.OrdersGridView.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.OrdersGridView.MultiSelect = false;
            this.OrdersGridView.Name = "OrdersGridView";
            this.OrdersGridView.ReadOnly = true;
            this.OrdersGridView.Size = new System.Drawing.Size(456, 331);
            this.OrdersGridView.TabIndex = 1;
            this.OrdersGridView.VirtualMode = true;
            this.OrdersGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.OrdersGridView_CellClick);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.Frozen = true;
            this.Id.HeaderText = "شماره";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Width = 70;
            // 
            // CustomerName
            // 
            this.CustomerName.DataPropertyName = "CustomerName";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.CustomerName.DefaultCellStyle = dataGridViewCellStyle4;
            this.CustomerName.HeaderText = "نام مشتری";
            this.CustomerName.Name = "CustomerName";
            this.CustomerName.ReadOnly = true;
            this.CustomerName.Width = 150;
            // 
            // RegisterDatetime
            // 
            this.RegisterDatetime.DataPropertyName = "RegisterDatetime";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.RegisterDatetime.DefaultCellStyle = dataGridViewCellStyle5;
            this.RegisterDatetime.HeaderText = "تاریخ ثبت";
            this.RegisterDatetime.Name = "RegisterDatetime";
            this.RegisterDatetime.ReadOnly = true;
            this.RegisterDatetime.Width = 90;
            // 
            // Ammount
            // 
            this.Ammount.DataPropertyName = "Ammount";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            dataGridViewCellStyle6.NullValue = "0";
            this.Ammount.DefaultCellStyle = dataGridViewCellStyle6;
            this.Ammount.HeaderText = "مبلغ فاکتور";
            this.Ammount.Name = "Ammount";
            this.Ammount.ReadOnly = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Btn_AddOrderItem);
            this.groupBox2.Controls.Add(this.Txb_Fee);
            this.groupBox2.Controls.Add(this.Txb_TotalPrice);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.Nud_Number);
            this.groupBox2.Controls.Add(this.Cmb_Goods);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(255, 18);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Size = new System.Drawing.Size(574, 102);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "موارد سفارش";
            // 
            // Btn_AddOrderItem
            // 
            this.Btn_AddOrderItem.Location = new System.Drawing.Point(6, 26);
            this.Btn_AddOrderItem.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Btn_AddOrderItem.Name = "Btn_AddOrderItem";
            this.Btn_AddOrderItem.Size = new System.Drawing.Size(105, 68);
            this.Btn_AddOrderItem.TabIndex = 9;
            this.Btn_AddOrderItem.Text = "اضافه نمودن";
            this.Btn_AddOrderItem.UseVisualStyleBackColor = true;
            this.Btn_AddOrderItem.Click += new System.EventHandler(this.Btn_AddOrderItem_Click);
            // 
            // Txb_Fee
            // 
            this.Txb_Fee.Enabled = false;
            this.Txb_Fee.Location = new System.Drawing.Point(341, 64);
            this.Txb_Fee.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Txb_Fee.Name = "Txb_Fee";
            this.Txb_Fee.Size = new System.Drawing.Size(154, 29);
            this.Txb_Fee.TabIndex = 8;
            // 
            // Txb_TotalPrice
            // 
            this.Txb_TotalPrice.Enabled = false;
            this.Txb_TotalPrice.Location = new System.Drawing.Point(117, 65);
            this.Txb_TotalPrice.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Txb_TotalPrice.Name = "Txb_TotalPrice";
            this.Txb_TotalPrice.Size = new System.Drawing.Size(154, 29);
            this.Txb_TotalPrice.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(277, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 22);
            this.label6.TabIndex = 6;
            this.label6.Text = "مبلغ کل:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(525, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 22);
            this.label4.TabIndex = 4;
            this.label4.Text = "فی:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(289, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 22);
            this.label3.TabIndex = 3;
            this.label3.Text = "تعداد:";
            // 
            // Nud_Number
            // 
            this.Nud_Number.Location = new System.Drawing.Point(117, 27);
            this.Nud_Number.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Nud_Number.Name = "Nud_Number";
            this.Nud_Number.Size = new System.Drawing.Size(154, 29);
            this.Nud_Number.TabIndex = 2;
            this.Nud_Number.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.Nud_Number.ValueChanged += new System.EventHandler(this.Nud_Number_ValueChanged);
            // 
            // Cmb_Goods
            // 
            this.Cmb_Goods.FormattingEnabled = true;
            this.Cmb_Goods.Location = new System.Drawing.Point(341, 26);
            this.Cmb_Goods.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Cmb_Goods.Name = "Cmb_Goods";
            this.Cmb_Goods.Size = new System.Drawing.Size(154, 30);
            this.Cmb_Goods.TabIndex = 1;
            this.Cmb_Goods.SelectionChangeCommitted += new System.EventHandler(this.Cmb_GoodsType_SelectionChangeCommitted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(501, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 22);
            this.label2.TabIndex = 0;
            this.label2.Text = "نوع کالا:";
            // 
            // OrderItems_GridView
            // 
            this.OrderItems_GridView.AllowUserToAddRows = false;
            this.OrderItems_GridView.AllowUserToDeleteRows = false;
            this.OrderItems_GridView.AllowUserToResizeRows = false;
            this.OrderItems_GridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OrderItems_GridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.OrderItems_GridView.Location = new System.Drawing.Point(474, 275);
            this.OrderItems_GridView.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.OrderItems_GridView.Name = "OrderItems_GridView";
            this.OrderItems_GridView.ReadOnly = true;
            this.OrderItems_GridView.Size = new System.Drawing.Size(355, 331);
            this.OrderItems_GridView.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Number";
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "تعداد";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Goods";
            this.dataGridViewTextBoxColumn2.HeaderText = "نوع کالا";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "TotalPrice";
            this.dataGridViewTextBoxColumn3.HeaderText = "مبلغ سفارش";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.BTN_Report);
            this.groupBox3.Controls.Add(this.DT_EndDate);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.DT_StartDate);
            this.groupBox3.Location = new System.Drawing.Point(12, 208);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox3.Size = new System.Drawing.Size(512, 59);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "تنظیم تاریخ";
            // 
            // BTN_Report
            // 
            this.BTN_Report.Location = new System.Drawing.Point(7, 22);
            this.BTN_Report.Name = "BTN_Report";
            this.BTN_Report.Size = new System.Drawing.Size(75, 30);
            this.BTN_Report.TabIndex = 4;
            this.BTN_Report.Text = "گزارش";
            this.BTN_Report.UseVisualStyleBackColor = true;
            this.BTN_Report.Click += new System.EventHandler(this.BTN_Report_Click);
            // 
            // DT_EndDate
            // 
            this.DT_EndDate.CalendarRightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DT_EndDate.Location = new System.Drawing.Point(88, 22);
            this.DT_EndDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DT_EndDate.Name = "DT_EndDate";
            this.DT_EndDate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DT_EndDate.Size = new System.Drawing.Size(120, 30);
            this.DT_EndDate.TabIndex = 3;
            this.DT_EndDate.UsePersianFormat = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(214, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 22);
            this.label7.TabIndex = 2;
            this.label7.Text = "تاریخ پایان:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("IRANSansWeb", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(424, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 22);
            this.label5.TabIndex = 1;
            this.label5.Text = "تاریخ شروع:";
            // 
            // DT_StartDate
            // 
            this.DT_StartDate.CalendarRightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DT_StartDate.Location = new System.Drawing.Point(295, 22);
            this.DT_StartDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DT_StartDate.Name = "DT_StartDate";
            this.DT_StartDate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DT_StartDate.Size = new System.Drawing.Size(126, 30);
            this.DT_StartDate.TabIndex = 0;
            this.DT_StartDate.UsePersianFormat = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button2);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.textBox2);
            this.groupBox4.Controls.Add(this.textBox1);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Location = new System.Drawing.Point(12, 127);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(488, 73);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "موارد مصرفی";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(6, 31);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(80, 29);
            this.button2.TabIndex = 4;
            this.button2.Text = "ثبت";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(243, 34);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 22);
            this.label9.TabIndex = 3;
            this.label9.Text = "هزینه:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(105, 31);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(132, 29);
            this.textBox2.TabIndex = 2;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(291, 31);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(133, 29);
            this.textBox1.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(430, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 22);
            this.label8.TabIndex = 0;
            this.label8.Text = "جنس:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.Txb_SalesAmmount);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Location = new System.Drawing.Point(530, 208);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(299, 59);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "حساب روز";
            // 
            // Txb_SalesAmmount
            // 
            this.Txb_SalesAmmount.Enabled = false;
            this.Txb_SalesAmmount.Location = new System.Drawing.Point(130, 22);
            this.Txb_SalesAmmount.Name = "Txb_SalesAmmount";
            this.Txb_SalesAmmount.Size = new System.Drawing.Size(100, 29);
            this.Txb_SalesAmmount.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(236, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 22);
            this.label10.TabIndex = 0;
            this.label10.Text = "فروش:";
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(506, 127);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(323, 73);
            this.listView1.TabIndex = 7;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // FrmOrders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 617);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.OrderItems_GridView);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.OrdersGridView);
            this.Font = new System.Drawing.Font("IRANSansWeb", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "FrmOrders";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "مدیریت سفارشات";
            this.Load += new System.EventHandler(this.Frm_Orders_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OrdersGridView)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Nud_Number)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderItems_GridView)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox TXB_CustomerName;
        private System.Windows.Forms.Button Btn_AddOrder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView OrdersGridView;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox Cmb_Goods;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Txb_Fee;
        private System.Windows.Forms.TextBox Txb_TotalPrice;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown Nud_Number;
        private System.Windows.Forms.Button Btn_AddOrderItem;
        private System.Windows.Forms.DataGridView OrderItems_GridView;
        private System.Windows.Forms.GroupBox groupBox3;
        private Atf.UI.DateTimeSelector DT_StartDate;
        private Atf.UI.DateTimeSelector DT_EndDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button Btn_FinalRegisterOrder;
        private System.Windows.Forms.Button BTN_Report;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustomerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn RegisterDatetime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ammount;
        private System.Windows.Forms.TextBox Txb_SalesAmmount;
        private System.Windows.Forms.Label label10;
    }
}