﻿using System;
using System.Globalization;

namespace Framework.Application
{
    public static class DatetimeConvertor
    {
        public static string ConvertToPersianDate(DateTime dateTime)
        {
            var persianCalendar = new PersianCalendar();
            return
                $"{persianCalendar.GetYear(dateTime)}/{persianCalendar.GetMonth(dateTime)}/{persianCalendar.GetDayOfMonth(dateTime)}";
        }

        public static DateTime ConvertToGregorianDate(string value, int hour)
        {
            var datetime = DateTime.Parse(value);
            var gregorianDate = new GregorianCalendar();
            var persianCalendar = new PersianCalendar();
            return new DateTime(gregorianDate.GetYear(datetime), gregorianDate.GetMonth(datetime),
                gregorianDate.GetDayOfMonth(datetime), hour, 00, 00, persianCalendar);
        }
    }
}