﻿using SalesManagement.Domain.Orders;

namespace SalesManagement.Domain.Tests.Utils
{
    public class OrderItemBuilder
    {
        public long Number { get; set; }
        public long Goods { get; set; }
        public double Price { get; set; }

        public OrderItemBuilder()
        {
            Number = 5;
            Goods = 3;
            Price = 60;
        }

        public OrderItemBuilder WithNumber(long number)
        {
            Number = number;
            return this;
        }

        public OrderItemBuilder WithGoods(long goods)
        {
            Goods = goods;
            return this;
        }

        public OrderItemBuilder WithUnitPrice(double unitPrice)
        {
            Price = unitPrice;
            return this;
        }

        public OrderItem Build()
        {
            return new OrderItem(Number, Goods, Price);
        }
    }
}
