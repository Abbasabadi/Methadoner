﻿using Framework.Core.Events;

namespace SalesManagement.Domain.Tests.Utils
{
    public class FakePublisher : IEventPublisher
    {
        public void Publish<T>(T eventToPublish) where T : IEvent
        {
        }
    }
}