﻿using System;
using System.Collections.Generic;
using Framework.Core.Events;
using SalesManagement.Domain.Orders;
using SalesManagement.Domain.Tests.Utils.Clock;

namespace SalesManagement.Domain.Tests.Utils
{
    public class OrderBuilder
    {
        public long Id { get; set; }
        public string CustomerName { get; set; }
        public List<OrderItem> Items { get; set; }
        public DateTime RegisterDatetime { get; set; }
        public IEventPublisher Publisher { get; set; }

        private readonly IClock _clock = new Clock.Clock();

        public OrderBuilder()
        {
            Id = 1;
            CustomerName = "hossein abbasabadi";
            Items = new List<OrderItem>();
            RegisterDatetime = _clock.FutureDateTime();
            Publisher = new FakePublisher();
        }

        public OrderBuilder WithId(long id)
        {
            Id = id;
            return this;
        }

        public OrderBuilder WithCustomerName(string customerName)
        {
            CustomerName = customerName;
            return this;
        }

        public OrderBuilder WithItems(List<OrderItem> items)
        {
            Items = items;
            return this;
        }

        public OrderBuilder WithRegisterDatetime(DateTime registerDateTime)
        {
            RegisterDatetime = registerDateTime;
            return this;
        }

        public OrderBuilder WithPublisher(IEventPublisher publisher)
        {
            Publisher = publisher;
            return this;
        }

        public Order Build()
        {
            var id = new OrderId(Id);
            return new Order(id, CustomerName, RegisterDatetime, Publisher);
        }
    }
}