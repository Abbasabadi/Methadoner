﻿using System;

namespace SalesManagement.Domain.Tests.Utils.Clock
{
    public interface IClock
    {
        DateTime FutureDateTime();
        DateTime PastDateTime();
    }
}
