﻿using System.Collections.Generic;
using SalesManagement.Domain.Orders;

namespace SalesManagement.Domain.Tests.Utils
{
    public static class ListOrderItemBuilder
    {
        public static List<OrderItem> CreateOrderItems(int count)
        {
            var orderItemBuilder = new OrderItemBuilder();
            var orderItems = new List<OrderItem>();
            for (var i = 0; i < count; i++)
            {
                orderItems.Add(orderItemBuilder.Build());
            }

            return orderItems;
        }
    }
}