﻿using SalesManagement.Domain.Orders.Exceptions;
using SalesManagement.Domain.Tests.Utils;
using Xunit;

namespace SalesManagement.Domain.Tests.Unit
{
    public class OrderItemTests
    {
        private readonly OrderItemBuilder _orderItemBuilder;

        public OrderItemTests()
        {
            _orderItemBuilder = new OrderItemBuilder();
        }

        [Fact]
        public void Constructor_Should_Construct_OrderItem_Properly()
        {
            //Act
            var orderItem = _orderItemBuilder.Build();

            //Assert
            Assert.Equal(_orderItemBuilder.Number, orderItem.Number);
            Assert.Equal(_orderItemBuilder.Goods, orderItem.Goods);
            Assert.Equal(_orderItemBuilder.Price, orderItem.TotalPrice);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-5)]
        public void Constructor_Should_Throw_Exception_When_Number_Is_Less_Than_Or_Equal_Zero(long number)
        {
            //Arrange
            _orderItemBuilder.WithNumber(number);

            //Assert
            Assert.Throws<NumberIsLessThanOrEqualZero>(() => _orderItemBuilder.Build());
        }


        [Fact]
        public void Constructor_Should_Throw_Exception_When_Price_Is_Less_Than_Zero()
        {
            //Arrange
            _orderItemBuilder.WithUnitPrice(-50);

            //Assert
            Assert.Throws<PriceIsLessThanZero>(() => _orderItemBuilder.Build());
        }
    }
}