﻿using Framework.Core.Events;
using Moq;
using SalesManagement.Domain.Tests.Utils;
using SalesManagement.Domain.Tests.Utils.Clock;
using Xunit;

namespace SalesManagement.Domain.Tests.Unit
{
    public class OrderTests
    {
        private readonly IClock _clock;
        private readonly OrderBuilder _orderBuilder;

        public OrderTests()
        {
            _clock = new Clock();
            _orderBuilder = new OrderBuilder();
        }

        [Fact]
        public void Constructor_Should_Construct_Order_Properly()
        {
            //Act
            var order = _orderBuilder.Build();

            //Assert
            Assert.Equal(_orderBuilder.Id, order.EntityId.DbId);
            Assert.Equal(_orderBuilder.CustomerName, order.CustomerName);
            Assert.Equal(_orderBuilder.RegisterDatetime, order.RegisterDatetime);
        }
        
        [Fact]
        public void Constructor_Should_Provide_Empty_Items()
        {
            //Arrange
            var order = _orderBuilder.Build();

            //Assert
            Assert.Empty(order.Items);
        }
        
        [Fact]
        public void AddItem_Should_Add_One_New_Item_To_OrderItems()
        {
            //Arrange
            var currentItems = ListOrderItemBuilder.CreateOrderItems(1);
            var orderItem = new OrderItemBuilder().Build();
            var order = _orderBuilder.WithItems(currentItems).Build();

            //Act
            order.AddItem(orderItem);

            //Assert
            Assert.Equal(_orderBuilder.Items.Count, order.Items.Count);
        }

        [Fact]
        public void Constructor_Should_Publish_OrderCreated()
        {
            var publisher = new Mock<IEventPublisher>();
            _orderBuilder.WithPublisher(publisher.Object);

            _orderBuilder.Build();

            publisher.Verify(x => x.Publish(It.IsAny<DomainEvent>()));
        }
    }
}