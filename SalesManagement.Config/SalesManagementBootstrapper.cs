﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Framework.Application;
using Framework.Nh;
using NHibernate;
using SalesManagement.Application;
using SalesManagement.Domain.Orders;
using SalesManagement.Interface.Facade;
using SalesManagement.Interface.Facade.Contracts;
using SalesManagement.Interface.Facade.Query;
using SalesManagement.Interface.Facade.Query.Order;
using SalesManagement.Persistance.Nh;
using SalesManagement.Persistance.Nh.Mappings;

namespace SalesManagement.Config
{
    public static class SalesManagementBootstrapper
    {
        public static void WireUp(IWindsorContainer container)
        {
            container.Register(Component.For<IOrderRepository>().ImplementedBy<OrderRepository>());

            container.Register(Component.For<IOrderCommandFacade>().ImplementedBy<OrderCommandFacade>());

            container.Register(Component.For<IOrderQueryFacade>().ImplementedBy<OrderQueryFacade>());

            container.Register(Classes.FromAssemblyContaining<OrderCommandHandler>().BasedOn(typeof(ICommandHandler<>))
                .WithService.AllInterfaces().LifestyleTransient());

            var sessionFactory =
                SessionFactoryBuilder.CreateByConnectionStringName("DBConnection", typeof(OrderMapping).Assembly);

            container.Register(Component.For<ISession>()
                .UsingFactoryMethod(a => sessionFactory.OpenSession()));
        }
    }
}