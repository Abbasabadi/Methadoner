﻿using System;
using System.Collections.Generic;
using SalesManagement.Interface.Facade.Contracts.Query;

namespace SalesManagement.Interface.Facade.Query.Order
{
    public interface IOrderQueryFacade
    {
        List<OrderDto> GetOrders(DateTime startDate, DateTime endDate);
        List<GoodsDto> GetGoods();
        OrderDto GetOrder(long id);
    }
}