﻿using System.Collections.Generic;
using System.Linq;
using Framework.Core;
using SalesManagement.Domain.Orders;
using SalesManagement.Interface.Facade.Contracts.Query;

namespace SalesManagement.Interface.Facade.Query.Order
{
    public static class OrderMappers
    {
        public static OrderDto Map(Domain.Orders.Order order)
        {
            var orderItems = order.Items.ToList();
            double ammount = 0;
            orderItems.ForEach(item => { ammount = ammount + item.TotalPrice; });
            return new OrderDto {
                Id = order.EntityId.DbId,
                CustomerName = order.CustomerName,
                RegisterDatetime = DatetimeConverter.ConvertToPersianDate(order.RegisterDatetime),
                Items = Map(orderItems),
                Ammount = ammount
            };
        }

        public static List<OrderDto> Map(List<Domain.Orders.Order> orders)
        {
            return orders.Select(Map).ToList();
        }

        public static List<OrderItemsDto> Map(List<OrderItem> orderItems)
        {
            return orderItems.Select(Map).ToList();
        }

        public static OrderItemsDto Map(OrderItem orderItem)
        {
            return new OrderItemsDto {
                Goods = SeedGoods.StaticGoods().First(a => a.Id == orderItem.Goods).Name,
                Number = orderItem.Number,
                TotalPrice = orderItem.TotalPrice
            };
        }
    }
}