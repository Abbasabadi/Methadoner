﻿using System.Collections.Generic;
using SalesManagement.Interface.Facade.Contracts.Query;

namespace SalesManagement.Interface.Facade.Query.Order
{
    public static class SeedGoods
    {
        public static List<GoodsDto> StaticGoods()
        {
            return new List<GoodsDto> {
                new GoodsDto(0, "", 0),
                new GoodsDto(1, "متادون", 5000),
                new GoodsDto(2, "قرص", 4000),
                new GoodsDto(3, "دارو", 8000)
            };
        }
    }
}
