﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using SalesManagement.Domain.Orders;
using SalesManagement.Interface.Facade.Contracts.Query;

namespace SalesManagement.Interface.Facade.Query.Order
{
    public class OrderQueryFacade : IOrderQueryFacade
    {
        private readonly ISession _session;

        public OrderQueryFacade(ISession session)
        {
            _session = session;
        }

        public List<GoodsDto> GetGoods()
        {
            return SeedGoods.StaticGoods();
        }

        public OrderDto GetOrder(long id)
        {
            var orderId = new OrderId(id);
            var order = _session.Get<Domain.Orders.Order>(orderId);
            return OrderMappers.Map(order);
        }

        public List<OrderDto> GetOrders(DateTime startDate, DateTime endDate)
        {
            var orders = _session.CreateCriteria<Domain.Orders.Order>()
                .Add(Restrictions.Ge("RegisterDatetime", startDate))
                .Add(Restrictions.Le("RegisterDatetime", endDate))
                .AddOrder(NHibernate.Criterion.Order.Desc("RegisterDatetime"))
                .List<Domain.Orders.Order>().ToList();
            //.Where(a => a.RegisterDatetime >= startDate)
            //.Where(a => a.RegisterDatetime <= endDate)
            return OrderMappers.Map(orders);
        }

        public List<OrderItemsDto> GetOrderItems(long orderId)
        {
            var orderItems = _session.Query<Domain.Orders.Order>().Single(a => a.EntityId.DbId == orderId).Items
                .ToList();
            return OrderMappers.Map(orderItems);
        }
    }
}