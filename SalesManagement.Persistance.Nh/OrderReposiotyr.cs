﻿using Framework.Nh;
using NHibernate;
using SalesManagement.Domain.Orders;

namespace SalesManagement.Persistance.Nh
{
    public class OrderRepository : IOrderRepository
    {
        private readonly ISession _session;

        public OrderRepository(ISession session)
        {
            _session = session;
        }

        public void AddOrder(Order order)
        {
            _session.Save(order);
        }

        public void Modify(Order order)
        {
            _session.Delete(order);
            _session.Save(order);
        }

        public Order GetBy(OrderId id)
        {
            return _session.Get<Order>(id);
        }

        public long GetNextId()
        {
            return _session.GetNextSequence("OrderSeq");
        }
    }
}