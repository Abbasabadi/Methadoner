﻿using Framework.Application;
using Framework.Core.Events;
using SalesManagement.Domain.Orders;
using SalesManagement.Interface.Facade.Contracts.Command;

namespace SalesManagement.Application
{
    public class OrderCommandHandler : ICommandHandler<AddOrder>, ICommandHandler<AddOrderItem>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IEventPublisher _publisher;

        public OrderCommandHandler(IOrderRepository orderRepository, IEventPublisher publisher)
        {
            _orderRepository = orderRepository;
            _publisher = publisher;
        }

        public void Handle(AddOrder command)
        {
            var id = _orderRepository.GetNextId();
            var orderId = new OrderId(id);
            var order = new Order(orderId, command.CustomerName, command.RegisterDatetime, _publisher);
            _orderRepository.AddOrder(order);
        }

        public void Handle(AddOrderItem command)
        {
            var orderItem = new OrderItem(command.Number, command.Goods, command.TotalPrice);
            var orderId = new OrderId(command.OrderId);
            var order = _orderRepository.GetBy(orderId);
            order.AddItem(orderItem);
            _orderRepository.Modify(order);
        }
    }
}