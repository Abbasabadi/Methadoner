﻿using System;
using Framework.Core.Events;

namespace SalesManagement.Domain.Contract
{
    public class OrderCreated : DomainEvent
    {
        public long Id { get; set; }

        public OrderCreated(long id)
        {
            Id = id;
        }
    }   
}