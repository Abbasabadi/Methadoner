using System;

namespace Framework.Core.Events
{
    public  abstract  class DomainEvent : IEvent
    {
        public Guid EventId { get; }
        public DateTime PublishDateTime { get; }
        protected DomainEvent()
        {
            EventId = Guid.NewGuid();
            PublishDateTime = DateTime.Now;
        }
    }
}