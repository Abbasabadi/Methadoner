﻿using System.Collections.Generic;
using System.Linq;

namespace Framework.Core.Events
{
    public class EventAggregator : IEventPublisher, IEventListener
    {
        private readonly List<object> _subscribers = new List<object>();
        public void Publish<T>(T eventToPublish) where T : IEvent
        {
            var handlers = _subscribers.OfType<IEventHandler<T>>().ToList();
            handlers.ForEach(handler =>
            {
                handler.Handle(eventToPublish);
            });
        }

        public void Subscribe<T>(IEventHandler<T> handler) where T : IEvent
        {
            _subscribers.Add(handler);
        }

        public void Unsubscribe<T>(IEventHandler<T> handler) where T : IEvent
        {
            _subscribers.Remove(handler);
        }
    }
}