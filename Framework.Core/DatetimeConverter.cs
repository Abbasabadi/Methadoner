﻿using System;
using System.Globalization;

namespace Framework.Core
{
    public static class DatetimeConverter
    {
        public static string ConvertToPersianDate(DateTime dateTime)
        {
            var persianCalendar = new PersianCalendar();
            return
                $"{persianCalendar.GetYear(dateTime)}/{persianCalendar.GetMonth(dateTime)}/{persianCalendar.GetDayOfMonth(dateTime)}";
        }
    }
}