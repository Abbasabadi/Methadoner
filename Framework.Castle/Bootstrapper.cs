﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Framework.Application;
using Framework.Core;
using Framework.Core.Events;
using Framework.Nh;

namespace Framework.Castle
{
    public static class Bootstrapper
    {
        public static void WireUp(IWindsorContainer container)
        {
            ServiceLocator.SetCurrent(new WindsorServiceLocator(container));

            container.Register(Component.For(typeof(TransactionalCommandHandlerDecorator<>)).LifestyleTransient());
            container.Register(Component.For<ICommandBus>().ImplementedBy<CommandBus>().LifestyleSingleton());

            //container.Register(Component.For<IQueryBus>().ImplementedBy<QueryBus>().LifestyleSingleton());

            container.Register(Component.For<IUnitOfWork>().ImplementedBy<NhUnitOfWork>());

            container.Register(Component.For<IEventListener>().Forward<IEventPublisher>().ImplementedBy<EventAggregator>());
        }
    }
}