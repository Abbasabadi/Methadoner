﻿using Framework.Core.Events;
using Moq;
using SalesManagement.Domain.Orders;
using SalesManagement.Domain.Tests.Utils;
using SalesManagement.Domain.Tests.Utils.Clock;
using SalesManagement.Interface.Facade.Contracts.Command;
using Xunit;

namespace SalesManagement.Application.Test.Unit
{
    public class OrderCommandHandlerTests
    {
        private readonly IClock _clock;
        private readonly Mock<IOrderRepository> _orderRepository;
        private readonly OrderCommandHandler _handler;

        public OrderCommandHandlerTests()
        {
            _clock = new Clock();
            _orderRepository = new Mock<IOrderRepository>();
            var publisher = new Mock<IEventPublisher>();
            _handler = new OrderCommandHandler(_orderRepository.Object, publisher.Object);
        }

        [Fact]
        public void Should_Call_AddOrder_When_AddOrder_Command_Passed()
        {
            //Arrange
            var command = new AddOrder("Ali Alvi", _clock.PastDateTime());
            _orderRepository.Setup(a => a.GetNextId()).Returns(It.IsAny<long>);

            //Act
            _handler.Handle(command);

            //Assert
            _orderRepository.Verify(z => z.GetNextId());
            _orderRepository.Verify(a => a.AddOrder(It.IsAny<Order>()));
        }


        [Fact]
        public void AddOrderItemsCommandHandler_Should_Update_OrderItems_On_Order_When_AddOrderItem_Passed()
        {
            //Arrange
            var order = new OrderBuilder().Build();
            var command = new AddOrderItem {OrderId = order.EntityId.DbId, Goods = 1, Number = 5, TotalPrice = 6000};
            _orderRepository.Setup(a => a.GetBy(order.EntityId)).Returns(order);

            //Act
            _handler.Handle(command);

            //Assert
            _orderRepository.Verify(a => a.GetBy(order.EntityId));
            _orderRepository.Verify(a=>a.Modify(order));
        }
    }
}