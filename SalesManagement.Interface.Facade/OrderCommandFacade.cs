﻿using Framework.Application;
using Framework.Core.Events;
using SalesManagement.Domain.Contract;
using SalesManagement.Interface.Facade.Contracts;
using SalesManagement.Interface.Facade.Contracts.Command;

namespace SalesManagement.Interface.Facade
{
    public class OrderCommandFacade : IOrderCommandFacade
    {
        private readonly ICommandBus _commandBus;
        private readonly IEventListener _eventListener;

        public OrderCommandFacade(ICommandBus commandBus, IEventListener eventListener)
        {
            _commandBus = commandBus;
            _eventListener = eventListener;
        }

        public long AddOrder(AddOrder command)
        {
            long id = 0;
            _eventListener.Subscribe(new ActionEventHandler<OrderCreated>(a => id = a.Id));
            _commandBus.Dispatch(command);
            return id;
        }

        public void AddOrderItem(AddOrderItem command)
        {
            _commandBus.Dispatch(command);
        }
    }
}